----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.01.2016 17:21:58
-- Design Name: 
-- Module Name: xlx_k7v7_phaligner_mmcm_controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity alt_a10_tx_phaligner is
  Port ( 
      -- Reset
      RESET_IN              : in std_logic;
      
      -- Clocks
      CLK_IN                : in std_logic;
      CLK_OUT               : out std_logic;
      
      -- Control
      SHIFT_IN              : in std_logic;
      SHIFT_COUNT_IN        : in std_logic_vector(7 downto 0);
      
      -- Status
      LOCKED_OUT            : out std_logic
  );
end alt_a10_tx_phaligner;

architecture Behavioral of alt_a10_tx_phaligner is
    -- COMPONENTS
    COMPONENT frameclk_pll
        PORT(
            cntsel              : in std_logic_vector(4 downto 0);
            locked              : out std_logic;
            num_phase_shifts    : in std_logic_vector(2 downto 0);
            outclk_0            : out std_logic;
            phase_done          : out std_logic;
            phase_en            : in std_logic;
            refclk              : in std_logic;
            rst                 : in std_logic;
            scanclk             : in std_logic;
            updn                : in std_logic
        );
    END COMPONENT;
   
    -- SIGNALS
    signal txPLLLocked_s               : std_logic;
	signal shift_to_mmcm               : std_logic;
    signal done_from_mmcm              : std_logic;
    signal phaseShiftCmd_to_ctrller    : std_logic;
    
    signal shiftDoneLatch              : std_logic;
    
begin

    --==================================--
    -- Tx PLL                           -- 
    --==================================--
    frameclk_pll_inst: frameclk_pll
		port map(
			locked   			=> txPLLLocked_s,
			outclk_0 			=> CLK_OUT,
			refclk   			=> CLK_IN,
			rst      			=> RESET_IN,
			
			cntsel           	=> "00000",
			num_phase_shifts 	=> "001",
			phase_done       	=> done_from_mmcm,
			scanclk          	=> CLK_IN,
			phase_en         	=> shift_to_mmcm,
			updn             	=> '0'
		);
        
    LOCKED_OUT <= shiftDoneLatch;
             
    phaligner_inst: entity work.phaligner_iopll_controller
        Port map(
          CLK_I                 => CLK_IN,
          RESET_I               => RESET_IN,
          
          PHASE_SHIFT_TO_MMCM   => shift_to_mmcm,
          SHIFT_DONE_FROM_MMCM  => done_from_mmcm,
          
          PHASE_SHIFT           => SHIFT_IN,
          SHIFT_DONE            => shiftDoneLatch,
          PLL_LOCKED            => txPLLLocked_s,
          
          SHIFT_CNTER           => SHIFT_COUNT_IN
       );
   
end Behavioral;
